-- Defines the child GitLab CI pipeline which builds our Docker images.

let
  Prelude = ./deps/Prelude.dhall
let
  Map = Prelude.Map
let
  CF = ./deps/Containerfile.dhall
let
  Image = ./Image.dhall
let
  GitLab = ./deps/GitLabCi.dhall

let
  images: List Image.Type = ./images.dhall

let
  mkJobEntry: Text -> GitLab.Job.Type -> Prelude.Map.Entry Text GitLab.Job.Type =
    Prelude.Map.keyValue GitLab.Job.Type

let
  toJob: Image.Type -> Prelude.Map.Entry Text GitLab.Job.Type =
    \(image: Image.Type) ->
    let 
      docker_base_url: Text = "registry.gitlab.haskell.org/$CI_PROJECT_PATH"
    let
      imageName: Text = "${docker_base_url}/${image.name}"

    -- N.B. https://docs.gitlab.com/ee/ci/docker/using_kaniko.html
    let job: GitLab.Job.Type =
      GitLab.Job::
      { image = Some (GitLab.Image ::
          { name = "gcr.io/kaniko-project/executor:debug"
          , entrypoint = Some [""]
          })
      , stage = Some image.jobStage
      , tags = Some image.runnerTags
      , variables = toMap
        { DOCKER_DRIVER = "overlay2"
        , DOCKER_TLS_CERTDIR = "/certs"
          -- See https://gitlab.haskell.org/ghc/ci-images/-/issues/3
        , container = "docker"
        }
      , needs = image.needs # [ "generate-dockerfiles" ]
      , dependencies = [ "generate-dockerfiles" ]
      , script =
        [ "mkdir -p /kaniko/.docker"
        , ''
          echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
          ''
        , ''
          /kaniko/executor \
            --context $CI_PROJECT_DIR \
            --dockerfile $CI_PROJECT_DIR/dockerfiles/${image.name}/Dockerfile \
            --destination $CI_REGISTRY_IMAGE/${image.name}:$CI_COMMIT_SHA \
            --destination $CI_REGISTRY_IMAGE/${image.name}:latest
          ''
        ]
      }

    in mkJobEntry image.name job

let
  dockerfilesJob =
    let
      dhallUrl = "https://github.com/dhall-lang/dhall-haskell/releases/download/1.34.0/dhall-1.34.0-x86_64-linux.tar.bz2"
    in GitLab.Job::
      { stage = Some "prepare"
      , image = Some (GitLab.Image :: { name = "debian:buster" })
      , tags = Some [ "x86_64-linux" ]
      , script = [
        , "mkdir -p dockerfiles"
        , "apt-get update"
        , "apt-get install -y curl tar bzip2"
        , "curl -L ${dhallUrl} | tar -jx"
        , "./bin/dhall to-directory-tree --file=dockerfiles.dhall --output=dockerfiles"
        ]
      , artifacts =
        Some (GitLab.ArtifactsSpec:: { paths = [ "dockerfiles" ] })
      }

let
  lintJob =
    GitLab.Job::
    { stage = Some "lint"
    , image = Some (GitLab.Image :: { name = "hadolint/hadolint:latest-debian" })
    , tags = Some [ "x86_64-linux" ]
    , script = 
      [ "find dockerfiles -name Dockerfile -print0 | xargs -0 -n1 hadolint" ]
    }

let
  jobs: Prelude.Map.Type Text GitLab.Job.Type =
    Prelude.List.map Image.Type (Prelude.Map.Entry Text GitLab.Job.Type) toJob images

let top: GitLab.Top.Type =
  GitLab.Top::
  -- N.B. Jobs sadly can only depend upon jobs in previous stages. Hence the
  -- `build-derived` stage, which exists only for the `linter` job.
  { stages = Some [ "prepare", "lint", "build", "build-derived" ]
  , jobs = jobs # toMap 
    { generate-dockerfiles = dockerfilesJob
    -- , lint = lintJob
    }
  }

in
Prelude.JSON.renderYAML (GitLab.Top.toJSON top)

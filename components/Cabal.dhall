let
  CF = ../deps/Containerfile.dhall

let
  setEnv: Text -> CF.Type =
    \(path: Text) ->
      CF.env (toMap { CABAL = path })

let
  installFromBindist: Text -> CF.Type =
    \(bindistUrl: Text) ->
      let
        path: Text = "/usr/local/bin/cabal"
      in
        CF.run "install cabal"
        [ "curl -L ${bindistUrl} | tar -Jx"
        , "mv cabal ${path}"
        , "${path} --version"
        ]
      # setEnv path

let 
  installFromSource: Text -> CF.Type =
    \(version: Text) ->
      let
        path: Text = "/usr/local/bin/cabal"
      in
        CF.run "install cabal"
        [ "cabal update"
        , "cabal install cabal-install==${version}"
        , "mv $HOME/.cabal/bin/cabal ${path}"
        , "${path} --version"
        ]
      # setEnv path

let
  type: Type =
    < FromBindist : Text
    | FromSource : Text
    | FromDistribution : Text
    >

let fromUpstreamBindist = 
  \(opts: { triple: Text, version: Text }) ->
      type.FromBindist "https://downloads.haskell.org/cabal/cabal-install-${opts.version}/cabal-install-${opts.version}-${opts.triple}.tar.xz"

let
  install: type -> CF.Type =
    \(src: type) ->
        merge
        { FromBindist = installFromBindist
        , FromSource = installFromSource
        , FromDistribution = setEnv
        } src

in
{ Type = type
, install = install
, fromUpstreamBindist = fromUpstreamBindist
}
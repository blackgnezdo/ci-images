-- Alpine Linux Docker images

let
  Prelude = ../deps/Prelude.dhall
let
  CF = ../deps/Containerfile.dhall
let
  HaskellTools = ../components/HaskellTools.dhall
let
  Cabal = ../components/Cabal.dhall
let
  Ghc = ../components/Ghc.dhall
let
  Image = ../Image.dhall

let
  coreBuildDepends: List Text =
    [ "autoconf"
    , "automake"
    , "binutils-gold"
    , "build-base"
    , "coreutils"
    , "cpio"
    , "linux-headers"
    , "libffi-dev"
    , "musl-dev"
    , "ncurses-dev"
    , "ncurses-static"
    , "python3"
    , "zlib-dev"
    , "xz"
    , "bash"
    , "git"
    , "wget"
    , "sudo"
    , "grep"
    , "curl"
    , "gmp-dev"
    , "cabal"
    , "ghc"
    ]

let
  docsBuildDepends: List Text =
    [ "py3-sphinx"
    , "texlive"
    , "texlive-xetex"
    , "texmf-dist-latexextra"
    , "ttf-dejavu"
    ]

let
  buildDepends: List Text = coreBuildDepends # docsBuildDepends

let installDepsStep: CF.Type =
      CF.run "Installing GHC build dependencies"
      [ "apk add --no-cache ${Prelude.Text.concatSep " " buildDepends}" ]

let
  ghcVersion: Text = "8.10.4"

let
  fetchGhcStep: CF.Type =
    let
      destDir: Text = "/opt/ghc/${ghcVersion}"
    in 
      Ghc.install
        { bindist = { version = ghcVersion, triple = "x86_64-alpine3.10-linux-integer-simple" }
        , destDir = destDir
        , configureOpts = [ "--disable-ld-override" ] : List Text
        }
      # CF.env (toMap { GHC = "${destDir}/bin/ghc" })

-- Create a normal user
let
  createUserStep: CF.Type =
      CF.run "create user"
      [ "adduser ghc --gecos 'GHC builds' --disabled-password"
      , "echo 'ghc ALL = NOPASSWD : ALL' > /etc/sudoers.d/ghc"
      ]
    # [CF.Statement.User "ghc"]
    # CF.workdir "/home/ghc/"

let
  images: List Image.Type =
  [ Image::
    { name = "x86_64-linux-alpine3_12"
    , runnerTags = [ "x86_64-linux" ]
    , image = 
        CF.from "alpine:3.12.0"
      # [ CF.Statement.Shell ["/bin/ash", "-eo", "pipefail", "-c"] ]
      # installDepsStep
      # fetchGhcStep
      # Cabal.install (Cabal.Type.FromDistribution "/usr/bin/cabal")
      # HaskellTools.build
      # createUserStep
      # CF.run "update cabal index" [ "$CABAL update"]
      # [ CF.Statement.Cmd ["bash"] ]
    }
  ]

in images

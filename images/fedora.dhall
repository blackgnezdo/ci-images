-- Fedora Linux Docker images

let
  Prelude = ../deps/Prelude.dhall
let
  CF = ../deps/Containerfile.dhall
let
  Llvm = ../components/Llvm.dhall
let
  Ghc = ../components/Ghc.dhall
let
  HaskellTools = ../components/HaskellTools.dhall
let
  Cabal = ../components/Cabal.dhall
let
  nsswitchWorkaround = ../components/NsswitchWorkaround.dhall
let
  Image = ../Image.dhall

let
  coreBuildDepends: List Text =
    [ "coreutils"
    , "binutils"
    , "which"
    , "git"
    , "make"
    , "automake"
    , "autoconf"
    , "gcc"
    , "perl"
    , "python3"
    , "xz"
    , "pxz"
    , "bzip2"
    , "lbzip2"
    , "patch"
    , "openssh-clients"
    , "sudo"
    , "zlib-devel"
    , "sqlite"
    , "ncurses-compat-libs"
    , "gmp-devel"
    , "ncurses-devel"
    , "gcc-c++"
    , "findutils"
    , "curl"
    , "wget"
    , "jq"
    , "elfutils-libs"
    , "elfutils-devel"
    -- For the dtrace code generator and headers
    , "systemtap-sdt-devel"
    ]

let
  docsBuildDepends: List Text =
    [ "python3-pip"
    , "texinfo"
    , "texlive"
    , "texlive-latex"
    , "texlive-xetex"
    , "texlive-collection-latex"
    , "texlive-collection-latexrecommended"
    , "texlive-xetex-def"
    , "texlive-collection-xetex"
    , "python-sphinx-latex"
    , "dejavu-sans-fonts"
    , "dejavu-serif-fonts"
    , "dejavu-sans-mono-fonts"
    ]

let
  buildDepends: List Text = coreBuildDepends # docsBuildDepends

let
  installPackages: List Text -> CF.Type =
    \(pkgs: List Text) ->
      CF.run "install build dependencies"
        [ "dnf -y install ${Prelude.Text.concatSep " " pkgs}" ]

-- Create a normal user
let
  createUserStep: CF.Type =
      CF.run "create user"
      [ "adduser ghc --comment 'GHC builds'"
      , "echo 'ghc ALL = NOPASSWD : ALL' > /etc/sudoers.d/ghc"
      ]
    # [CF.Statement.User "ghc"]
    # CF.workdir "/home/ghc/"

let
  FedoraImage =
    let
      type: Type =
        { name: Text
        , fromImage : Text
        , runnerTags : List Text
        , bootstrapLlvm : Optional Llvm.Source
        , bootstrapGhc : Ghc.BindistSpec
        , llvm : Optional Llvm.Source
        , cabalSource : Cabal.Type
        , extraPackages: List Text
        }

    let
      toDocker: type -> Image.Type = \(opts : type) ->
        let
          ghcDir: Text = "/opt/ghc/${opts.bootstrapGhc.version}"
        let
          bootLlvmDir: Text = "/opt/llvm-bootstrap"
        let
          llvmDir: Text = "/opt/llvm"
        let
          bootstrapLlvmConfigureOptions =
            merge { Some = \(_: Llvm.Source) -> [ "LLC=${bootLlvmDir}/bin/llc", "OPT=${bootLlvmDir}/bin/opt" ]
                  , None = [] : List Text
                  } opts.bootstrapLlvm
            
        let
          image =
            CF.from opts.fromImage
          # CF.env (toMap { LANG = "C.UTF-8" })
          # [ CF.Statement.Shell ["/bin/bash", "-o", "pipefail", "-c"] ]
          # installPackages (buildDepends # opts.extraPackages)

            -- install LLVM for bootstrap GHC
          # Llvm.maybeInstallTo bootLlvmDir opts.bootstrapLlvm

            -- install GHC
          # Ghc.install
              { bindist = opts.bootstrapGhc
              , destDir = ghcDir
              , configureOpts = bootstrapLlvmConfigureOptions
              }
          # CF.env (toMap { GHC = "${ghcDir}/bin/ghc" })

            -- install LLVM to be used by built compiler
          # Llvm.maybeInstallTo llvmDir opts.llvm
          # Llvm.setEnv llvmDir

            -- install cabal-install
          # Cabal.install opts.cabalSource

            -- install hscolour, alex, and happy
          # HaskellTools.build

          # nsswitchWorkaround
          # createUserStep
          # CF.run "update cabal index" [ "$CABAL update"]
          # [ CF.Statement.Cmd ["bash"] ]

        in Image::{ name = opts.name, runnerTags = opts.runnerTags, image = image }
    in
    { Type = type
    , toDocker = toDocker
    }

let images: List Image.Type =
[ FedoraImage.toDocker
    { name = "x86_64-linux-fedora27"
    , fromImage = "amd64/fedora:27"
    , runnerTags = [ "x86_64-linux" ]
    , bootstrapLlvm = None Llvm.Source
    , bootstrapGhc = { version = "8.10.4", triple = "x86_64-fedora27-linux" }
    , llvm = Some (Llvm.Source.FromBindist { version = "11.0.1" , triple = "x86_64-linux-gnu-ubuntu-16.04" })
    , cabalSource = Cabal.fromUpstreamBindist { version = "3.2.0.0", triple = "x86_64-unknown-linux" }
    , extraPackages = [] : List Text
    }

, FedoraImage.toDocker
    { name = "x86_64-linux-fedora32"
    , fromImage = "amd64/fedora:32"
    , runnerTags = [ "x86_64-linux" ]
    , bootstrapLlvm = None Llvm.Source
    , bootstrapGhc = { version = "8.10.4", triple = "x86_64-fedora27-linux" }
    , llvm = Some (Llvm.Source.FromBindist { version = "11.0.1" , triple = "x86_64-linux-gnu-ubuntu-16.04" })
    , cabalSource = Cabal.fromUpstreamBindist { version = "3.2.0.0", triple = "x86_64-unknown-linux" }
    , extraPackages = [] : List Text
    }

, FedoraImage.toDocker
    { name = "x86_64-linux-fedora33"
    , fromImage = "amd64/fedora:33"
    , runnerTags = [ "x86_64-linux" ]
    , bootstrapLlvm = None Llvm.Source
    , bootstrapGhc = { version = "8.10.4", triple = "x86_64-fedora27-linux" }
    , llvm = Some (Llvm.Source.FromBindist { version = "11.0.1" , triple = "x86_64-linux-gnu-ubuntu-16.04" })
    , cabalSource = Cabal.fromUpstreamBindist { version = "3.2.0.0", triple = "x86_64-unknown-linux" }
    , extraPackages = [] : List Text
    }
]
in images
